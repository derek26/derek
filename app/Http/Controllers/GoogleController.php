<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JanDrda\LaravelGoogleCustomSearchEngine\LaravelGoogleCustomSearchEngine;

class GoogleController extends Controller
{
    public function index(Request $request){
        $fulltext = new LaravelGoogleCustomSearchEngine(); // initialize
        $results = $fulltext->getResults($request->search); // get first 10 results for query 'some phrase'

        return json_decode(json_encode($results), true);

    }
    public function bingIndex(Request $request)
    {
        $accessKey = config('laravelGoogleCustomSearchEngine.bingApiKey');
        $endpoint = 'https://api.cognitive.microsoft.com/bingcustomsearch/v7.0';
        $term = 'Microsoft Cognitive Services';

        $headers = "Ocp-Apim-Subscription-Key".config('laravelGoogleCustomSearchEngine.bingApiKey');
        $options = array ('http' => array (
            'header' => $headers,
            'method' => 'GET'));

        // Perform the request and receive a response.
        $context = stream_context_create($options);
        $result = file_get_contents($endpoint . "?q=" . urlencode($request->bingSearch), false, $context);
        return json_decode(json_encode($result), true);
    }
}
